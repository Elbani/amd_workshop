package com.amd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {


    @Autowired
    UserRepository userRepository;

    @Autowired
    CompanyRepository companyRepository;

    @GetMapping("/accounts")
    List<Account> getAllAccounts() {
        return (List<Account>) userRepository.findAll();
    }


    @PostMapping("/accounts")
    public void saveAcount(@RequestBody Account account){
        userRepository.save(account);
    }

    @GetMapping("/companies")
    public List<Company> getCompanies(){
        return (List<Company>) companyRepository.findAll();
    }

    @PostMapping("/companies")
    public void saveCompany(@RequestBody Company company){
        companyRepository.save(company);
    }

}
